import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';

export const GET_DOGS = gql`
  query {
    dogs {
      id
    }
  }
`;

/**
 * The main application entrance point
 * @namespace Components/App
 */
export const App: React.FC = () => {
  const { data, called, loading, error } = useQuery(GET_DOGS);

  if (error) {
    return <div>{JSON.stringify(error)}</div>;
  }

  return <div>{called && !loading && JSON.stringify(data)}</div>;
};
