import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import { ApolloClient as Apollo } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from '@apollo/react-hooks';
import fetch from 'unfetch';

const ApolloClient = new Apollo({
  cache: new InMemoryCache(),
  link: new HttpLink({
    fetch,
    uri: 'https://dog-graphql-api.glitch.me/',
  }),
  name: 'Primary Client',
  version: '0.1',
});

ReactDOM.render(
  <ApolloProvider client={ApolloClient}>
    <App />
  </ApolloProvider>,
  document.getElementById('app'),
);
