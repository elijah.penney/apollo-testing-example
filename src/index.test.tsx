import React from 'react';
import { render, waitFor, act } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import { GET_DOGS, App } from './App';

const mocks = [
  {
    request: {
      query: GET_DOGS,
    },
    results: {
      data: {
        dogs: [
          {
            id: 'test',
          },
        ],
      },
    },
  },
];
act(() => {
  it('renders without error', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <App />
      </MockedProvider>,
    );
    /**
     * This is here to avoid the "You didn't wrap in act" error
     */
    await waitFor(() => {});
  });
});
