const { resolve } = require('path');

module.exports = {
  roots: ['<rootDir>/src'],
  testMatch: [
    '**/__tests__/**/*.+(ts|tsx|js)',
    '**/?(*.)+(spec|test).+(ts|tsx|js)',
  ],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
    '^.+\\.js$': 'babel-jest',
  },
  moduleNameMapper: {
    '^@testUtils': resolve(__dirname, './src/testUtils'),
    '^@stores': resolve(__dirname, './src/stores'),
    '^@components': resolve(__dirname, './src/components'),
    '^@pages/(.*)$': resolve(__dirname, './src/pages/$1'),
    '^@pages': resolve(__dirname, './src/pages'),
    '^@helpers': resolve(__dirname, './src/helpers'),
  },
};
