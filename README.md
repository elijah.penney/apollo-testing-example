Please see file `./src/components/PrivateRoute/\__tests\__/index.tsx`

Error:
```javascript
  console.error
    Warning: React.createElement: type is invalid -- expected a string (for built-in components) or a class/function (for composite components) but got: undefined. You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports.

      62 |     const rendered = render(
      63 |       <div>
    > 64 |         <MockedProvider mocks={mock}>
         |         ^
      65 |           <div data-testid='allow-private-route-component' />
      66 |         </MockedProvider>
      67 |       </div>,

      at printWarning (node_modules/react/cjs/react.development.js:315:30)
      at error (node_modules/react/cjs/react.development.js:287:5)
      at Object.createElementWithValidation [as createElement] (node_modules/react/cjs/react.development.js:1788:7)
      at Object.<anonymous> (src/components/PrivateRoute/__tests__/index.tsx:64:9)
```
